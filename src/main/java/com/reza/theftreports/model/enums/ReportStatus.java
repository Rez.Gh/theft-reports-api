package com.reza.theftreports.model.enums;

public enum ReportStatus {
    OPEN,CLOSED,UNDER_INVESTIGATION
}
