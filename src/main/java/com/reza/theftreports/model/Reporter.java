package com.reza.theftreports.model;

import lombok.*;
import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Reporter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String nationalCode;

    @Column(nullable = false)
    private String phoneNumber;

    @OneToMany(mappedBy = "reporter")
    private List<TheftReport> reports;
}
