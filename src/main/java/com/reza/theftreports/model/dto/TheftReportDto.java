package com.reza.theftreports.model.dto;

import lombok.*;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
public class TheftReportDto {

    private String documentNumber;

    private Long createdDate;

    private String description;

    private ReporterDto reporterDto;

}
