package com.reza.theftreports.model.dto;

import com.reza.theftreports.model.enums.ReportResultStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
public class UpdateReportDto {
    private Long id;
    private ReportResultStatus resultStatus;
}
