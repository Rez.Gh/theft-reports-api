package com.reza.theftreports.model.dto;

import lombok.*;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
public class ReporterDto {

    private String firstName;

    private String lastName;

    private String nationalCode;

    private String phoneNumber;
}
