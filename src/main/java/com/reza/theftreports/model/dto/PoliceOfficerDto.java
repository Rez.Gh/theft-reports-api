package com.reza.theftreports.model.dto;

import lombok.*;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
public class PoliceOfficerDto {

    private String firstName;

    private String lastName;

    private String personalCode;

}
