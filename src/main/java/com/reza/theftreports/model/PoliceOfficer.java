package com.reza.theftreports.model;

import com.reza.theftreports.model.enums.PoliceOfficerStatus;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PoliceOfficer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String personalCode;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PoliceOfficerStatus officerStatus;

    @OneToMany(mappedBy = "policeOfficer")
    private List<TheftReport> reports;


}
