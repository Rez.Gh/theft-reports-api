package com.reza.theftreports.model;

import com.reza.theftreports.model.enums.ReportResultStatus;
import com.reza.theftreports.model.enums.ReportStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TheftReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String documentNumber;


    @ManyToOne
    @JoinColumn(name = "reporter_id")
    private Reporter reporter;


    @ManyToOne
    @JoinColumn(name = "police_officer_id")
    private PoliceOfficer policeOfficer;

    @Column(nullable = false)
    private Long createdDate;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ReportStatus status;

    @Column()
    @Enumerated(EnumType.STRING)
    private ReportResultStatus result;

    @Column()
    private Long closedDate;

}
