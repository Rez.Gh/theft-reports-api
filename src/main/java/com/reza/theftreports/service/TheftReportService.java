package com.reza.theftreports.service;

import com.reza.theftreports.model.PoliceOfficer;
import com.reza.theftreports.model.Reporter;
import com.reza.theftreports.model.TheftReport;
import com.reza.theftreports.model.dto.TheftReportDto;
import com.reza.theftreports.model.enums.PoliceOfficerStatus;
import com.reza.theftreports.model.enums.ReportStatus;
import com.reza.theftreports.repository.PoliceOfficerRepository;
import com.reza.theftreports.repository.TheftReportRepository;
import com.reza.theftreports.utility.mapper.Mapper;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TheftReportService {
    private final TheftReportRepository reportRepository;
    private final PoliceOfficerRepository officerRepository;
    private final Mapper mapper;

    public TheftReport create(TheftReportDto reportDto, Reporter reporter) {
        TheftReport theftReport = mapper.theftReportDtoToTheftReport(reportDto);
        theftReport.setReporter(reporter);
        PoliceOfficer officer = getAvailableOfficer();
        theftReport.setPoliceOfficer(officer);
        officer.setOfficerStatus(PoliceOfficerStatus.BUSY);
        theftReport.setStatus(ReportStatus.UNDER_INVESTIGATION);
        officerRepository.save(officer);
        return reportRepository.save(theftReport);
    }

    public TheftReport getTheftReport(Long id) throws NotFoundException {
        Optional<TheftReport> theftReport = reportRepository.findById(id);
        if (theftReport.isPresent()) {
            return theftReport.get();
        } else {
            throw new NotFoundException("Not found");
        }
    }

    public List<TheftReport> getAllTheftReports() {

        return reportRepository.findAll();
    }

    public List<TheftReport> getOpenTheftReports() {

        return reportRepository.findAllByStatus(ReportStatus.OPEN);
    }

    public List<TheftReport> getClosedTheftReports() {

        return reportRepository.findAllByStatus(ReportStatus.CLOSED);
    }

    public List<TheftReport> getUnderInvestigationTheftReports() {

        return reportRepository.findAllByStatus(ReportStatus.UNDER_INVESTIGATION);
    }

    private PoliceOfficer getAvailableOfficer() {
        if (!reportRepository.findAll().isEmpty()){
            PoliceOfficer officer = officerRepository.getFirstByOfficerStatusAndReportsClosedDateLessThan(
                    PoliceOfficerStatus.FREE, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            return officer;
        }else {
            return officerRepository.findAllByOfficerStatus(PoliceOfficerStatus.FREE).stream().findFirst().get();
        }


    }

}
