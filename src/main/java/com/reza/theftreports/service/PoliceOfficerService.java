package com.reza.theftreports.service;

import com.reza.theftreports.model.PoliceOfficer;
import com.reza.theftreports.model.TheftReport;
import com.reza.theftreports.model.dto.PoliceOfficerDto;
import com.reza.theftreports.model.enums.PoliceOfficerStatus;
import com.reza.theftreports.repository.PoliceOfficerRepository;
import com.reza.theftreports.utility.mapper.Mapper;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@RequiredArgsConstructor
@Service
public class PoliceOfficerService {
    private final PoliceOfficerRepository officerRepository;
    private final Mapper mapper;

    public PoliceOfficer create(PoliceOfficerDto officerDto) {
        PoliceOfficer officer = mapper.officerDtoToPoliceOfficer(officerDto);
        officer.setOfficerStatus(PoliceOfficerStatus.FREE);
        return officerRepository.save(officer);
    }

    public PoliceOfficer getOfficer(Long id) throws NotFoundException {
        Optional<PoliceOfficer> policeOfficer = officerRepository.findById(id);
        if (policeOfficer.isPresent()) {
            return policeOfficer.get();
        } else {
            throw new NotFoundException("Not found");
        }
    }

    public List<PoliceOfficer> getAllOfficer() {

        return officerRepository.findAll();

    }

    public List<PoliceOfficer> getFreeOfficers() {

        return officerRepository.findAllByOfficerStatus(PoliceOfficerStatus.FREE);
    }

    public List<PoliceOfficer> getBusyOfficers() {

        return officerRepository.findAllByOfficerStatus(PoliceOfficerStatus.BUSY);
    }

    public void freeOfficer(TheftReport theftReport){
        PoliceOfficer officer = officerRepository.getOne(theftReport.getPoliceOfficer().getId());
        officer.setOfficerStatus(PoliceOfficerStatus.FREE);
        officerRepository.save(officer);

    }

}
