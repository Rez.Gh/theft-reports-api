package com.reza.theftreports.service;

import com.reza.theftreports.model.Reporter;
import com.reza.theftreports.model.dto.ReporterDto;
import com.reza.theftreports.repository.ReporterRepository;
import com.reza.theftreports.utility.mapper.Mapper;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ReporterService {

    private final ReporterRepository reporterRepository;
    private final Mapper mapper;

    public Reporter create(ReporterDto reporterDto){

        return reporterRepository.save(mapper.reporterDtoToReporter(reporterDto));

    }

    public Reporter getReporter(Long id) throws NotFoundException {
        Optional<Reporter> reporter = reporterRepository.findById(id);
        if (reporter.isPresent()){
            return reporter.get();
        }else {
            throw new NotFoundException("Not found");
        }
    }

}
