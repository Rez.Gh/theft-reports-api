package com.reza.theftreports.utility.mapper;

import com.reza.theftreports.model.PoliceOfficer;
import com.reza.theftreports.model.Reporter;
import com.reza.theftreports.model.TheftReport;
import com.reza.theftreports.model.dto.PoliceOfficerDto;
import com.reza.theftreports.model.dto.ReporterDto;
import com.reza.theftreports.model.dto.TheftReportDto;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@org.mapstruct.Mapper(componentModel = "spring")
public interface Mapper {
    Mapper MAPPER = Mappers.getMapper(Mapper.class);

    @Mapping(target = "officerStatus", defaultValue = "PoliceOfficerStatus.FREE")
    PoliceOfficer officerDtoToPoliceOfficer(PoliceOfficerDto officerDto);

    Reporter reporterDtoToReporter(ReporterDto reporterDto);

    @Mappings(@Mapping(target = "status",defaultValue =" ReportStatus.OPEN"))
    TheftReport theftReportDtoToTheftReport(TheftReportDto theftReportDto);

}
