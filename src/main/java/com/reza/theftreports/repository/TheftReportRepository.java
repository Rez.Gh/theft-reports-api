package com.reza.theftreports.repository;

import com.reza.theftreports.model.TheftReport;
import com.reza.theftreports.model.enums.ReportStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TheftReportRepository extends JpaRepository<TheftReport, Long> {

    List<TheftReport> findAllByStatus(ReportStatus status);

}
