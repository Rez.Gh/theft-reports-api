package com.reza.theftreports.repository;

import com.reza.theftreports.model.Reporter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReporterRepository extends JpaRepository<Reporter,Long> {

}
