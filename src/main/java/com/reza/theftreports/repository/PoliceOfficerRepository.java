package com.reza.theftreports.repository;

import com.reza.theftreports.model.PoliceOfficer;
import com.reza.theftreports.model.enums.PoliceOfficerStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PoliceOfficerRepository extends JpaRepository<PoliceOfficer,Long> {
    List<PoliceOfficer> findAllByOfficerStatus(PoliceOfficerStatus status);

    PoliceOfficer getFirstByOfficerStatusAndReportsClosedDateLessThan(PoliceOfficerStatus status, Long closedDate);

}
