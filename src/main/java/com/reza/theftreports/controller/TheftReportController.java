package com.reza.theftreports.controller;


import com.reza.theftreports.model.Reporter;
import com.reza.theftreports.model.TheftReport;
import com.reza.theftreports.model.dto.TheftReportDto;
import com.reza.theftreports.model.dto.UpdateReportDto;
import com.reza.theftreports.model.enums.ReportStatus;
import com.reza.theftreports.repository.ReporterRepository;
import com.reza.theftreports.repository.TheftReportRepository;
import com.reza.theftreports.service.PoliceOfficerService;
import com.reza.theftreports.service.ReporterService;
import com.reza.theftreports.service.TheftReportService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/report")
public class TheftReportController {
    private final TheftReportService reportService;
    private final ReporterService reporterService;
    private final TheftReportRepository reportRepository;
    private final PoliceOfficerService officerService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createReport(@RequestBody TheftReportDto reportDto){
        Reporter reporter =  reporterService.create(reportDto.getReporterDto());
        reportService.create(reportDto, reporter);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity closeReport(@RequestBody UpdateReportDto reportDto){
        try {
            TheftReport report = reportService.getTheftReport(reportDto.getId());
            report.setClosedDate(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            report.setStatus(ReportStatus.CLOSED);
            report.setResult(reportDto.getResultStatus());
            officerService.freeOfficer(report);
            reportRepository.save(report);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (NotFoundException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TheftReport>> getAllReport(){
        List<TheftReport> reports = reportService.getAllTheftReports();
        return ResponseEntity.status(HttpStatus.OK).body(reports);
    }

    @GetMapping(path = "open",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TheftReport>> getOpenReport(){
        List<TheftReport> reports = reportService.getOpenTheftReports();
        return ResponseEntity.status(HttpStatus.OK).body(reports);
    }

    @GetMapping(path = "closed",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TheftReport>> getClosedReport(){
        List<TheftReport> reports = reportService.getClosedTheftReports();
        return ResponseEntity.status(HttpStatus.OK).body(reports);
    }

    @GetMapping(path = "under-investigation",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TheftReport>> getUnderInvestigationReport(){
        List<TheftReport> reports = reportService.getUnderInvestigationTheftReports();
        return ResponseEntity.status(HttpStatus.OK).body(reports);
    }
}
