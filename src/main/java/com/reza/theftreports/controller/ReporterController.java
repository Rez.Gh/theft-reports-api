package com.reza.theftreports.controller;

import com.reza.theftreports.model.dto.ReporterDto;
import com.reza.theftreports.service.ReporterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/reporter")
public class ReporterController {
    private final ReporterService reporterService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createReporter(@RequestBody ReporterDto reporterDto){
        reporterService.create(reporterDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
