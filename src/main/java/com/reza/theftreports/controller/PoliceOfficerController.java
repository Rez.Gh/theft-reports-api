package com.reza.theftreports.controller;

import com.reza.theftreports.model.PoliceOfficer;
import com.reza.theftreports.model.dto.PoliceOfficerDto;
import com.reza.theftreports.service.PoliceOfficerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/officer")
public class PoliceOfficerController {
    private final PoliceOfficerService officerService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createOfficer(@RequestBody PoliceOfficerDto officerDto) {
        officerService.create(officerDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PoliceOfficer>> getAllOfficer(){
        List<PoliceOfficer> officers = officerService.getAllOfficer();
        return ResponseEntity.status(HttpStatus.OK).body(officers);
    }

    @GetMapping(path = "/free" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PoliceOfficer>> getFreeOfficer(){
        List<PoliceOfficer> officers = officerService.getFreeOfficers();
        return ResponseEntity.status(HttpStatus.OK).body(officers);
    }

    @GetMapping(path = "busy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PoliceOfficer>> getBusyOfficer(){
        List<PoliceOfficer> officers = officerService.getBusyOfficers();
        return ResponseEntity.status(HttpStatus.OK).body(officers);
    }
}
