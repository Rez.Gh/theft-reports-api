package com.reza.theftreports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheftReportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheftReportsApplication.class, args);
    }

}
